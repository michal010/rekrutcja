﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Attractor : MonoBehaviour
{
    public Rigidbody rb;

    public static event Action OnAttractorEnabled = delegate { };
    public static event Action OnAttractorDisabled = delegate { };

    public float GravityFieldRadius;
    bool attract = true;

    public void ResetAttributes()
    {
        transform.Resize(1);
        rb.mass = UniversalGravitySettings.Instance.BaseMass;
        GravityFieldRadius = UniversalGravitySettings.Instance.BaseGravityRadius;
    }
    private void OnEnable()
    {
        OnAttractorEnabled();
    }
    private void OnDisable()
    {
        OnAttractorDisabled();
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb = gameObject.GetComponent<Rigidbody>();
        rb.mass = UniversalGravitySettings.Instance.BaseMass;
        GravityFieldRadius = UniversalGravitySettings.Instance.BaseGravityRadius;
    }
    private void FixedUpdate()
    {
        if (!attract)
            return;
        foreach (var el in GetAttractorsInField(GravityFieldRadius))
        {
            if (el == this)
                continue;
            Attract(el);
        }
    }

    public void ExplosionBehaviour(Vector3 force)
    {
        StartCoroutine(TimeEnableCollison(UniversalGravitySettings.Instance.NoCollisionTime));
        //rb.AddRandomForceInRandomDirectionXYZ(UniversalGravitySettings.Instance.OnMassTreshholdReachedMinForce, UniversalGravitySettings.Instance.OnMassTreshholdReachedMaxForce);
        rb.AddForce(force);
    }
    private IEnumerator TimeEnableCollison(float time)
    {
        attract = false;
        yield return new WaitForSeconds(time);
        GetComponent<Collider>().isTrigger = false;
        attract = true;
    }

    private void CombineAttractors(Attractor otherAttractor)
    {
        if(CheckMassTreshhold())
        {
            //Destroy(otherAttractor.gameObject);
            //Destroy(gameObject);
            SphereCreator.Instance.EnqueueObject(otherAttractor.gameObject);
            SphereCreator.Instance.EnqueueObject(gameObject);
            SphereCreator.Instance.SpawnSpheresWithRandomForce(UniversalGravitySettings.Instance.OnMassTreshholdReachedNewSphereCount, transform.position);

        }
        else
        {
            float newRadius = AttractorUtils.CalculateRadiusBySurfaceArea(
                    AttractorUtils.CalculateSurfaceArea(otherAttractor.transform) +
                    AttractorUtils.CalculateSurfaceArea(transform)
                    );
            rb.transform.Resize(newRadius);
            rb.mass += otherAttractor.transform.GetComponent<Rigidbody>().mass;
            GravityFieldRadius += otherAttractor.GravityFieldRadius;
            //Destroy(otherAttractor.gameObject);
            SphereCreator.Instance.EnqueueObject(otherAttractor.gameObject);
        }

    }



    private bool CheckMassTreshhold()
    {
        if (rb.mass >= UniversalGravitySettings.Instance.BaseMass * UniversalGravitySettings.Instance.MassTreshhold)
            return true;
        else
            return false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!UniversalGravitySettings.AttractorsCanCombine)
            return;
        Attractor attractor = collision.collider.GetComponent<Attractor>();
        if (attractor == null)
            return;
        if (attractor.rb.mass == rb.mass)
        {
            if (attractor.GetInstanceID() > GetInstanceID())
                return;
            else
            {
                CombineAttractors(attractor);
            }
        }
        else if(attractor.rb.mass < rb.mass)
        {
            CombineAttractors(attractor);
        }
        else
        {
            return;
        }
    }

    private List<Attractor> GetAttractorsInField(float radius)
    {
        Collider[] colliders = Physics.OverlapSphere(this.transform.position, GravityFieldRadius);
        List<Attractor> result = new List<Attractor>();
        for (int i = 0; i < colliders.Length; i++)
        {
            result.Add(colliders[i].GetComponent<Attractor>());
        }
        return result;
    }

    private void Attract(Attractor a)
    {
        if (a == null)
            return;
        Vector3 direction = rb.position - a.rb.position;
        float distance = direction.magnitude;
        if (distance < 0.01f)
            return;
        float f = UniversalGravitySettings.Instance.GravityScale * UniversalGravitySettings.G * (rb.mass * a.rb.mass) / Mathf.Pow(distance, 2);
        Vector3 force = direction.normalized * f * (int)UniversalGravitySettings.ForceType;
        a.rb.AddForce(force);
    }
}
