﻿using UnityEngine;

public static class TransformExtensions
{
    public static void Resize(this Transform t, float x)
    {
        t.localScale = new Vector3(x, x, x);
    }
}

public static class RigidbodyExtensions
{
    public static void AddRandomForceInRandomDirectionXYZ(this Rigidbody rb, float minForce, float maxForce)
    {
        Vector3 ForceDirection = UnityEngine.Random.insideUnitSphere;
        Debug.Log("Force dir: " + ForceDirection);
        float Force = UnityEngine.Random.Range(minForce, maxForce);
        Debug.Log("Force: " + Force*ForceDirection);
        rb.AddForce(Force * ForceDirection);
    }

    public static Vector3 PickRandomPointInSpereRadius(float radius)
    {
        return UnityEngine.Random.insideUnitSphere * radius;
    }
}


public static class AttractorUtils 
{

    public static float GetRadius(Transform sphere)
    {
        return sphere.localScale.x;
    }
    public static float CalculateSurfaceArea(Transform sphere)
    {
        float radius = GetRadius(sphere);
        return 4 * Mathf.PI * radius * radius;
    }

    public static float CalculateRadiusBySurfaceArea(float SurfaceArea)
    {
        return Mathf.Sqrt(SurfaceArea / (4 * Mathf.PI));
    }

    public static void Resize()
    {

    }

}
