﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UniversalForceType { Push = -1, Pull = 1} 

public class UniversalGravitySettings : MonoBehaviour
{
    [Header("Bazowy promień pola grawitacyjnego")]
    [Range(10,30)]
    public  float BaseGravityRadius = 30;
    public static float G = 6.67e-11f;
    [Header("Skala grawitacji w odniesienu do rzeczywistości")]
    public float GravityScale = 500000f;
    [Header("Bazowa masa sfery")]
    public float BaseMass = 100000f;
    [Header("Mnożnik massy przy sprawdzaniu 'wybuchu'")]
    public int MassTreshhold = 50;

    [Header("Ilość tworzonych sfer w momencie 'wybuchu'")]
    public int OnMassTreshholdReachedNewSphereCount = 50;
    [Header("Minimalna moc wybuchu")]
    public int OnMassTreshholdExplosionMinForce = 10000;
    [Header("Maksymalna moc wybuchu")]
    public int OnMassTreshholdExplosionMaxForce = 20000;
    [Header("Czas braku kolicji po 'wybuchu'")]
    public float NoCollisionTime = 0.5f;

    public static UniversalForceType ForceType { get; private set; } = UniversalForceType.Pull;
    public static bool AttractorsCanCombine { get; private set; } = true;

    public static void SetAttractorsCombineFlag(bool value)
    {
        AttractorsCanCombine = value;
    }
    public static void SetFoce(UniversalForceType type)
    {
        ForceType = type;
    }

    public static UniversalGravitySettings Instance;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
}
