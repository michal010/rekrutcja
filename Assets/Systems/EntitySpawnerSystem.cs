﻿//using Unity.Entities;
//using Unity.Transforms;
//using Unity.Mathematics;
//using Unity.Rendering;
//using System.Collections.Generic;
//using System.Linq;

//public class EntitySpawnerSystem : ComponentSystem
//{
//    private float spawnTimer;
//    public static int SpheresCount = 0;
//    public static List<Entity> Spheres = new List<Entity>();
//    public static EntitySpawnerSystem Instance;
//    private static bool initialized = false;

//    protected override void OnCreate()
//    {
//        if(Instance == null)
//        {
//            Instance = this;
//        }

//    }

//    protected override void OnUpdate()
//    {
//        if (!SphereCreator.Instance.UseECS)
//            return;
//        if(!initialized)
//        {
//            for (int i = 0; i < SphereCreator.Instance.SpheresLimit; i++)
//            {
//                var position = SphereCreator.Instance.GetRandomPointInCameraPOV();
//                Spawn(position);
//            }
//            initialized = true;
//        }
//    }

//    public void Spawn(UnityEngine.Vector3 position, float mass = 10000f,float radius = 1f)
//    {
//        Entity spawnedEntity = EntityManager.Instantiate(PrefabEntities.SphereEntity);
//        //EntityManager.AddComponentData<SphereColliderComponent>(spawnedEntity, new SphereColliderComponent { isActive = true, radius = radius });
//        //EntityManager.AddComponentData<RigidbodyComponent>(spawnedEntity, new RigidbodyComponent { Velocity = new float3(0, 0, 0), Drag = 0.05f, Mass = mass });
//        //EntityManager.AddComponentData<AttractorComponent>(spawnedEntity, new AttractorComponent { });
//        //EntityManager.AddComponentData<CompositeScale>(spawnedEntity, new CompositeScale { Value = float4x4.Scale(radius) });
//        //EntityManager.AddComponentData<DestructableComponent>(spawnedEntity, new DestructableComponent { destroyOnUpdate = false });
//        //EntityManager.SetComponentData(spawnedEntity, new Translation { Value = new float3(position.x, position.y, position.z) });
//        SpheresCount++;
//    }

//}
