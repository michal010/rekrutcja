﻿//using Unity.Burst;
//using Unity.Entities;
//using Unity.Jobs;
//using Unity.Mathematics;
//using Unity.Transforms;

//public class PhysicsSystem : JobComponentSystem
//{
//    EntityQuery rigidbodyGroup;

//    protected override void OnCreate()
//    {
//    }

//    protected override JobHandle OnUpdate(JobHandle inputDeps)
//    {
//        rigidbodyGroup = GetEntityQuery(ComponentType.ReadWrite<RigidbodyComponent>(), ComponentType.ReadWrite<Translation>());
//        var rigidbodyType = GetArchetypeChunkComponentType<RigidbodyComponent>(false);
//        var translationType = GetArchetypeChunkComponentType<Translation>(false);

//        var physicJob = new PhysicsJob()
//        {
//            deltaTime = Time.DeltaTime,
//            rigidbodyType = rigidbodyType,
//            translationType = translationType
//        };
//        JobHandle jobHandle = physicJob.Schedule(rigidbodyGroup,inputDeps);

//        return jobHandle;

//    }

//    [BurstCompile]
//    struct PhysicsJob : IJobChunk
//    {
//        public float deltaTime;
//        public ArchetypeChunkComponentType<RigidbodyComponent> rigidbodyType;
//        public ArchetypeChunkComponentType<Translation> translationType;
//        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
//        {
//            var chunkTranslations = chunk.GetNativeArray(translationType);
//            var chunkRigidbodies = chunk.GetNativeArray(rigidbodyType);
//            for (int i = 0; i < chunk.Count; i++)
//            {
//                //CalculateNewPosition(out chunkTranslations[i], chunkRigidbodies[i], deltaTime);
//                float3 oldValue = chunkTranslations[i].Value;

//                RigidbodyComponent CachedRb = chunkRigidbodies[i];

//                //if (CachedRb.GetSpeed() < 0.1f)
//                //{
//                //    return;
//                //}

//                float velocityScaling = math.mul(1-deltaTime, 1-chunkRigidbodies[i].Drag);
//                RigidbodyComponent newRigidbodyComponent = new RigidbodyComponent
//                {
//                    Drag = CachedRb.Drag,
//                    Mass = CachedRb.Mass,
//                    Velocity = new float3(
//                        math.mul(CachedRb.Velocity.x, velocityScaling),
//                        math.mul(CachedRb.Velocity.y, velocityScaling),
//                        math.mul(CachedRb.Velocity.z, velocityScaling)
//                        ),
//                    Speed = CachedRb.GetSpeed()
//                };

//                chunkRigidbodies[i] = newRigidbodyComponent;

//                chunkTranslations[i] = new Translation
//                {
//                    Value = new float3(
                        
//                        math.mul(CachedRb.Velocity.x, deltaTime) + oldValue.x,
//                        math.mul(CachedRb.Velocity.y, deltaTime) + oldValue.y,
//                        math.mul(CachedRb.Velocity.z, deltaTime) + oldValue.z
//                        ) 
//                };
//            }
//        }

//        static void CalculateNewPosition(ref Translation translation, RigidbodyComponent rigidbody, float deltaTime)
//        {
//            translation.Value = rigidbody.Velocity * deltaTime;
//        }
//    }
//}


