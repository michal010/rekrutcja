﻿//using Unity.Burst;
//using Unity.Entities;
//using Unity.Jobs;
//using Unity.Mathematics;
//using Unity.Transforms;

//public class UniversalGravitySystem : JobComponentSystem
//{
//    EntityQuery attractorGroup;
//    //change to some static settings file.
//    private static readonly float G = 6.67e-11f;
//    private static readonly float ForceScale = 100000f;

//    protected override void OnCreate()
//    {
        
//    }

//    protected override JobHandle OnUpdate(JobHandle inputDeps)
//    {
//        attractorGroup = GetEntityQuery(ComponentType.ReadOnly<AttractorComponent>(), ComponentType.ReadOnly<RigidbodyComponent>(), ComponentType.ReadOnly<Translation>());
//        var rigidbodyType = GetArchetypeChunkComponentType<RigidbodyComponent>(false);
//        var translationType = GetArchetypeChunkComponentType<Translation>(false);

//        var attractJob = new AttractJob()
//        {
//            deltaTime = Time.DeltaTime,
//            rigidbodyType = rigidbodyType,
//            translationType = translationType
//        };

//        JobHandle jobHandle = attractJob.Schedule(attractorGroup, inputDeps);
//        return jobHandle;

//    }



//    [BurstCompile]
//    struct AttractJob : IJobChunk
//    {
//        public float deltaTime;
//        public ArchetypeChunkComponentType<RigidbodyComponent> rigidbodyType;
//        public ArchetypeChunkComponentType<Translation> translationType;

//        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
//        {
//            var chunkTranslations = chunk.GetNativeArray(translationType);
//            var chunkRigidbodies = chunk.GetNativeArray(rigidbodyType);


//            for (int i = 0; i < chunk.Count; i++)
//            {
//                for (int j = 0; j < chunk.Count; j++)
//                {
//                    if (i == j)
//                        continue;
//                    //attract
//                    float3 direction = new float3(
//                        chunkTranslations[i].Value.x - chunkTranslations[j].Value.x,
//                        chunkTranslations[i].Value.y - chunkTranslations[j].Value.y,
//                        chunkTranslations[i].Value.z - chunkTranslations[j].Value.z
//                        );

//                    direction = math.normalize(direction);

//                    float distance = math.distance(chunkTranslations[i].Value, chunkTranslations[j].Value);
//                    if (distance < 1f)
//                        continue;

//                    //calc force
//                    float f = math.mul(  ForceScale, math.mul(G, math.mul (chunkRigidbodies[i].Mass, chunkRigidbodies[j].Mass))) / math.mul(distance,distance);
//                    float3 force = direction * f * deltaTime;
//                    //apply force

//                    RigidbodyComponent oldRB = chunkRigidbodies[j];



//                    chunkRigidbodies[j] = new RigidbodyComponent {
//                        Drag = oldRB.Drag,
//                        Mass = oldRB.Mass,
//                        Speed = oldRB.Speed,
//                        Velocity = new float3(oldRB.Velocity.x + force.x, oldRB.Velocity.y + force.y, oldRB.Velocity.z + force.z)
//                    };

//                    //.AddForce();

//                }
//            }
//        }

//    }
//}
