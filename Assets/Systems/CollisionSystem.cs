﻿//using System.ComponentModel;
//using Unity.Burst;
//using Unity.Collections;
//using Unity.Entities;
//using Unity.Jobs;
//using Unity.Mathematics;
//using Unity.Transforms;
//using UnityEngine;

//public class CollisionSystem : JobComponentSystem
//{
//    EntityQuery sphereGroup;

//    protected override void OnCreate()
//    {
//    }
//    protected override JobHandle OnUpdate(JobHandle inputDeps)
//    {
//        sphereGroup = GetEntityQuery(ComponentType.ReadOnly<Translation>(), ComponentType.ReadOnly<SphereColliderComponent>(), ComponentType.ReadOnly<RigidbodyComponent>(), ComponentType.ReadWrite<CompositeScale>());
//        var translationType = GetArchetypeChunkComponentType<Translation>();
//        var sphereColliderType = GetArchetypeChunkComponentType<SphereColliderComponent>();
//        var rigidbodyType = GetArchetypeChunkComponentType<RigidbodyComponent>();
//        var compositeScaleType = GetArchetypeChunkComponentType<CompositeScale>();
//        var destructableType = GetArchetypeChunkComponentType<DestructableComponent>();
//        var jobCvC = new CollisionJob()
//        {
//            translationType = translationType,
//            sphereColliderType = sphereColliderType,
//            rigidbodyType = rigidbodyType,
//            compositeScaleType = compositeScaleType,
//            destructableType = destructableType

//        };
//        JobHandle jobHandle = jobCvC.Schedule(sphereGroup, inputDeps);
//        return jobHandle;

//    }


//    [BurstCompile]
//    struct CollisionJob : IJobChunk
//    {
//        public ArchetypeChunkComponentType<Translation> translationType;
//        public ArchetypeChunkComponentType<SphereColliderComponent> sphereColliderType;
//        public ArchetypeChunkComponentType<RigidbodyComponent> rigidbodyType;
//        public ArchetypeChunkComponentType<CompositeScale> compositeScaleType;
//        public ArchetypeChunkComponentType<DestructableComponent> destructableType;

//        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
//        {
//            var chunkTranslations = chunk.GetNativeArray(translationType);
//            var chunkSphereColliders = chunk.GetNativeArray(sphereColliderType);
//            var chunkRigidbodies = chunk.GetNativeArray(rigidbodyType);
//            var chunkCompositeScales = chunk.GetNativeArray(compositeScaleType);
//            var chunkDestructable = chunk.GetNativeArray(destructableType);
//            for (int i = 0; i < chunk.Count; i++)
//            {
//                if (chunkDestructable[i].destroyOnUpdate) continue;
//                for (int j = 0; j < chunk.Count; j++)
//                {
//                    if (i == j) continue;
//                    if (chunkDestructable[j].destroyOnUpdate) continue;
//                    float radius = chunkSphereColliders[i].radius + chunkSphereColliders[j].radius;
//                    radius = radius * radius;
//                    //Check collision 
//                    if (CheckCollision(chunkTranslations[i].Value, chunkTranslations[j].Value, radius))
//                    {

//                        Debug.Log("Halo");
//                        //CombineAttractors(out chunkCompositeScales[i], chunkCompositeScales[j], chunkRigidbodies[i], chunkRigidbodies[j], chunkDestructable[j]);

//                        float CombinedSurfaceArea = CalculateSurfaceArea(chunkCompositeScales[i].Value.c0.x) + CalculateSurfaceArea(chunkCompositeScales[j].Value.c0.x);
//                        float newScale = CalculateRadiusBySurfaceArea(CombinedSurfaceArea);

//                        RigidbodyComponent oldRB = chunkRigidbodies[i];

//                        chunkRigidbodies[i] = new RigidbodyComponent
//                        {
//                            Drag = oldRB.Drag,
//                            Mass = oldRB.Mass + chunkRigidbodies[j].Mass,
//                            Speed = oldRB.Speed,
//                            Velocity = oldRB.Velocity
//                        };
//                        chunkCompositeScales[i] = new CompositeScale { Value = float4x4.Scale(newScale) };
//                        chunkSphereColliders[i] = new SphereColliderComponent { isActive = true, radius = newScale / 2 };
//                        chunkDestructable[j] = new DestructableComponent { destroyOnUpdate = true };

//                        //if(CanCombine(i,j, chunkRigidbodies[i].Mass, chunkRigidbodies[j].Mass))
//                        //{
//                        //    Debug.Log("Should combine");
//                        //}
//                    }

//                }
//            }

//        }

//        public bool CanCombine(int i, int j, float m1, float m2)
//        {
//            if (m1 == m2)
//            {
//                if (i > j)
//                    return true;
//                else
//                {
//                    return false;
//                }
//            }
//            else if (m1 > m2)
//            {
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//        }

//        public void CombineAttractors(ref CompositeScale Scale1, CompositeScale Scale2, ref RigidbodyComponent r1, RigidbodyComponent r2, ref DestructableComponent destroy)
//        {
//            //destory j
//            //combine j to i

//            float CombinedSurfaceArea = CalculateSurfaceArea(Scale1.Value.c0.x) + CalculateSurfaceArea(Scale2.Value.c0.x);
//            float newScale = CalculateRadiusBySurfaceArea(CombinedSurfaceArea);

//            RigidbodyComponent oldRB = r1;

//            r1 = new RigidbodyComponent
//            {
//                Drag = oldRB.Drag,
//                Mass = oldRB.Mass + r2.Mass,
//                Speed = oldRB.Speed,
//                Velocity = oldRB.Velocity
//            };
//            Scale1 = new CompositeScale { Value = float4x4.Scale(newScale) };
//            destroy = new DestructableComponent { destroyOnUpdate = true };

//        }
//        private static float CalculateSurfaceArea(float radius)
//        {
//            return 4 * math.PI * radius * radius;
//        }

//        private static float CalculateRadiusBySurfaceArea(float SurfaceArea)
//        {
//            return Mathf.Sqrt(SurfaceArea / (4 * Mathf.PI));
//        }

//    }




//    static bool CheckCollision(float3 posA, float3 posB, float radiusSqr)
//    {
//        float3 delta = posA - posB;

//        float distanceSquare = delta.x * delta.x + delta.y * delta.y + delta.z * delta.z;

//        return distanceSquare <= radiusSqr;
//    }

//}
