﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public TextMeshProUGUI SpheresCountTextRef;


    private void Awake()
    {
        Attractor.OnAttractorEnabled+= UpdateSphereCountText;
        Attractor.OnAttractorDisabled += UpdateSphereCountText;
    }

    public void UpdateSphereCountText()
    {
        SpheresCountTextRef.text = string.Format("Spheres: {0}", SphereCreator.EnabledSpheres);
    }
}
